/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.filelab;

import java.io.Serializable;

/**
 *
 * @author BankMMT
 */
public class Player implements Serializable{

    private char symbol;
    private int win = 0;
    private int loss = 0;
    private int draw = 0;

    public Player(char symbol) {
        this.symbol = symbol;

    }

    public char getSymbol() {
        return symbol;
    }

    public void setSymbol(char Symbol) {
        this.symbol = symbol;
    }

    public int getWin() {
        return win;
    }

    public void win() {
        this.win++;
    }

    public int getLoss() {
        return loss;
    }

    public void loss() {
        this.loss++;
    }

    public int getDraw() {
        return draw;
    }

    public void draw() {
        this.draw++;
    }

    @Override
    public String toString() {
        return "Player {" + "Symbol = " + symbol + " ,Win : " + win + " ,Loss : " + loss + " ,Draw : " + draw;
    }
}
